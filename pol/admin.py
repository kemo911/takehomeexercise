from django.contrib import admin
from .models import Polygon


@admin.register(Polygon)
class PolAdmin(admin.ModelAdmin):
    list_display = ('internal_id', 'name', 'external_id', 'category', 'avg_rating')
    search_fields = ('internal_id', 'external_id')
    list_filter = ('category',)

import csv
import json
import xml.etree.ElementTree as ElementTree
from django.core.management.base import BaseCommand
from pol.models import Polygon


class Command(BaseCommand):
    help = 'Import Polygons from a file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str)

    def handle(self, *args, **options):
        file_path = options['file_path']
        if file_path.endswith('.csv'):
            self.import_csv(file_path)
        elif file_path.endswith('.json'):
            self.import_json(file_path)
        elif file_path.endswith('.xml'):
            self.import_xml(file_path)
        else:
            self.stdout.write(self.style.ERROR('Unsupported file extension.'))

    def get_or_update_polygon(self, external_id, defaults):
        polygon, created = Polygon.objects.get_or_create(
            external_id=external_id,
            defaults=defaults
        )
        if not created:
            self.stdout.write(self.style.WARNING(f'Polygon with external_id {external_id} already exists.'))
            action = input('Do you want to skip or update the existing record? (Skip/update): ').strip().lower()
            if action == 'update':
                Polygon.objects.filter(external_id=external_id).update(**defaults)
                self.stdout.write(self.style.SUCCESS(f'Polygon with external_id {external_id} has been updated.'))
            else:
                self.stdout.write(self.style.SUCCESS(f'Skipped polygon with external_id {external_id}.'))
        else:
            self.stdout.write(self.style.SUCCESS(f'Polygon with external_id {external_id} has been created.'))

    def import_csv(self, file_path):
        with open(file_path, mode='r') as file:
            reader = csv.DictReader(file)
            for record in reader:
                defaults = {
                    'name': record['pol_name'],
                    'latitude': record['pol_latitude'],
                    'longitude': record['pol_longitude'],
                    'category': record['pol_category'],
                    'avg_rating': record['pol_ratings']
                }
                self.get_or_update_polygon(record['pol_id'], defaults)

    def import_json(self, file_path):
        with open(file_path, mode='r') as file:
            records = json.load(file)
            for record in records:
                defaults = {
                    'name': record['name'],
                    'latitude': record['coordinates']['latitude'],
                    'longitude': record['coordinates']['longitude'],
                    'category': record['category'],
                    'avg_rating': record['ratings']
                }
                self.get_or_update_polygon(record['id'], defaults)

    def import_xml(self, file_path):
        tree = ElementTree.parse(file_path)
        tree_root = tree.getroot()
        for record in tree_root.findall('polygon'):
            defaults = {
                'name': record.find('pname').text,
                'latitude': float(record.find('platitude').text),
                'longitude': float(record.find('plongitude').text),
                'category': record.find('pcategory').text,
                'avg_rating': float(record.find('pratings').text)
            }
            self.get_or_update_polygon(record.find('pid').text, defaults)
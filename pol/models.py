import uuid
from django.db import models


class Polygon(models.Model):
    internal_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    external_id = models.CharField(max_length=255, unique=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    category = models.CharField(max_length=255)
    avg_rating = models.FloatField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Polygon'
        verbose_name_plural = 'Polygons'

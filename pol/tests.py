from django.core.management import call_command
from django.test import TestCase
from unittest.mock import patch, mock_open
from pol.models import Polygon


def get_sample_csv_data():
    return """pol_id,pol_name,pol_latitude,pol_longitude,pol_category,pol_ratings
1,Sample Place,40.748817,-73.985428,Landmark,4.5
"""


def get_sample_json_data():
    return ('[{"id": "1", "name": "Sample Place", "coordinates": {"latitude": 40.748817, "longitude": -73.985428}, '
            '"category": "Landmark", "ratings": 4.5}]')


def get_sample_xml_data():
    return """<?xml version="1.0"?>
<polygons>
    <polygon>
        <pid>1</pid>
        <pname>Sample Place</pname>
        <platitude>40.748817</platitude>
        <plongitude>-73.985428</plongitude>
        <pcategory>Landmark</pcategory>
        <pratings>4.5</pratings>
    </polygon>
</polygons>
"""


class ImportPolygonTestCase(TestCase):
    @patch('builtins.open', new_callable=mock_open, read_data=get_sample_csv_data())
    def test_import_csv_creates_records(self, mock_file):
        call_command('import_pol', 'dummy.csv')
        self.assertEqual(Polygon.objects.count(), 1)

    @patch('builtins.open', new_callable=mock_open, read_data=get_sample_json_data())
    def test_import_json_creates_records(self, mock_file):
        call_command('import_pol', 'dummy.json')
        self.assertEqual(Polygon.objects.count(), 1)

    @patch('builtins.open', new_callable=mock_open, read_data=get_sample_xml_data())
    def test_import_xml_creates_records(self, mock_file):
        call_command('import_pol', 'dummy.xml')
        self.assertEqual(Polygon.objects.count(), 1)

    @patch('builtins.input', return_value='skip')
    @patch('builtins.open', new_callable=mock_open, read_data=get_sample_csv_data())
    def test_skip_existing_records(self, mock_file, mock_input):
        Polygon.objects.create(external_id="1", name="Existing Place", latitude=0.0, longitude=0.0, category="Existing",
                               avg_rating=0.0)
        call_command('import_pol', 'dummy.csv')
        self.assertEqual(Polygon.objects.count(), 1)  # No new record should be added

    @patch('builtins.input', return_value='update')
    @patch('builtins.open', new_callable=mock_open, read_data=get_sample_csv_data())
    def test_update_existing_records(self, mock_file, mock_input):
        Polygon.objects.create(external_id="1", name="Existing Place", latitude=0.0, longitude=0.0, category="Existing",
                               avg_rating=0.0)
        call_command('import_pol', 'dummy.csv')
        polygon = Polygon.objects.get(external_id="1")
        self.assertNotEqual(polygon.name, "Existing Place")  # Assuming the mock data has a different name
